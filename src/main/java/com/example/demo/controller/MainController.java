package com.example.demo.controller;

import com.example.demo.domain.entity.User;
import com.example.demo.domain.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component
@Path("/")
public class MainController {

    @Autowired
    private UserRepository repo;

    @GET
    @Produces("text/json")
    @Path("/user")
    public Response main( @DefaultValue("0") @QueryParam("id") long id) {
        User user = repo.findOne(id).orElseThrow(() -> new RuntimeException("Fuck this all"));
        return Response.ok().entity(user).type(MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Produces("text/json")
    @Path("/hello")
    public String hello() {
        return "Hello world";
    }

}
