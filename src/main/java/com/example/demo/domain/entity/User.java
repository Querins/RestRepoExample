package com.example.demo.domain.entity;

import com.example.demo.domain.entity.utils.UserDeserializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.hibernate.validator.constraints.Email;

import javax.persistence.*;

@Entity
public class User {

    public User() {}

    @Id
    private long id;
    private String name;
    private String username;
    @Email
    private String email;
    @JsonProperty("zipcode")
    private String zipCode;
    //@OneToOne
    //private UserCoordinates geo;
    private String phone;
    private String website;
    //@ManyToOne(cascade = CascadeType.ALL)
    //private Company company;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }


}
