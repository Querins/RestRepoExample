package com.example.demo.domain.repository;

import com.example.demo.domain.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

@Repository
public class UserRepositoryImpl implements CustomizedFindOne<User, Long> {

    private RestTemplate rest;

    @Autowired
    public UserRepositoryImpl(RestTemplate restTemplate) {
        this.rest = restTemplate;
    }

    @Override
    public Optional<User> findOne(Long id) {
        try {
            User[] users = rest.getForObject(new URI("https://jsonplaceholder.typicode.com/users?id=" + id), User[].class);
            return Optional.of(users[0]);
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException("Bad uri syntax ", e);
        }
    }

}
