package com.example.demo.domain.repository;

import com.example.demo.domain.entity.User;
import org.springframework.data.repository.Repository;

import java.util.Optional;

public interface CustomizedFindOne<T, PK> {

    Optional<T> findOne(PK id);

}
